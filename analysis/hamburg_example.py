import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import re
from random import random

fluence = 79. #1MeV neq cm^-2
D1_filenames = ["F512_D1_CV_IV_0min_-20C_Parallel.dat",
                "F512_D1_CV_IV_20mins@60C_-20C_Parallel.dat",
                "F512_D1_CV_IV_40mins@60C_-20C_Parallel.dat",
                "F512_D1_CV_IV_50mins@60C_-20C_Parallel.dat",
                "F512_D1_CV_IV_55mins@60C_-20C_Parallel.dat",
                "F512_D1_CV_IV_60mins@60C_-20C_Parallel.dat",
                "F512_D1_CV_IV_65mins@60C_-20C_Parallel.dat",
                "F512_D1_CV_IV_70mins_-20C_Parallel.dat",
                "F512_D1_CV_IV_75mins_-20C_Parallel.dat",
                "F512_D1_CV_IV_100mins_-20C_Parallel.dat",
                "F512_D1_CV_IV_156mins_-20C_Parallel.dat",
                "F512_D1_CV_IV_312mins@60C_-20C_Parallel.dat"
                ]

filenames = D1_filenames
in_dir = "Plots/example_data"
title = "$N_{eff}$ vs. Annealing Time (F512_D1, 1kHz)"
fig_filename = "F512_D1_hamburg.png"

p0 = [1.,1.,1.,1.,1.]

# this assumes the first file in the list is the pre-irradiation measurement
N_eff_preirr = 0.
N_eff_preirr_err = 0.
with open(in_dir+'/'+filenames[0], "r") as f:
    contents = f.readlines()
    N_eff_row = contents[44].split()
    N_eff_preirr = float(N_eff_row[1])
    N_eff_preirr_err = float(N_eff_row[3])

# the example data does not include pre-irradiation measurements,
# so we set the pre-irradiation N_eff to 0.
# COMMENT OUT THESE LINES IF YOU HAVE PRE-IRRADIATION DATA
N_eff_preirr = 0.
N_eff_preirr_err = 0.
    
N_eff0s = []
N_eff_errs = []
times = []

# uncomment the following line and delete "for fn in filenames" if your data
# includes a pre-irradiation measurement, as that should be the first file
#for fn in filenames[1:]:
for fn in filenames:
    with open(in_dir+'/'+fn, "r") as f:
        contents = f.readlines()
        N_eff_row = contents[44].split()
        N_eff = float(N_eff_row[1])
        N_eff_err = float(N_eff_row[3])
        N_eff_errs.append(N_eff_err)
        N_eff0s.append(N_eff)
        time_row = contents[37].split()
        print ">>> time row is", time_row
        time = float(re.findall(r'\d+',time_row[1].split("@")[0])[0])
        #time = float(time_row[1].split("@")[0][:-3])
        print "time is", time
        times.append(time)

def general_fit(f, xdata, ydata, p0=None, sigma=None, **kw):
    
    popt, pcov = curve_fit(f, xdata, ydata, p0, sigma, maxfev=100000)
    if sigma is None:
        chi2 = sum(((f(xdata,*popt)-ydata))**2)
    else:
        chi2 = sum(((f(xdata,*popt)-ydata)/sigma)**2)
    dof = len(ydata) - len(popt)
    rchi2 = chi2/dof
    punc = np.zeros(len(popt))
    for i in np.arange(0,len(popt)):
        punc[i] = np.sqrt(pcov[i,i])
    return popt, punc, rchi2, dof 

def Hamburg(x, ga, NC, gy, ta, ty):
    return ga*np.exp(-x/ta)*fluence + gy*(1.-1./(1.+x/ty))*fluence + NC

diffs = [x - N_eff_preirr for x in N_eff0s]
diff_errs = [np.sqrt(x**2 + N_eff_preirr_err**2) for x in N_eff_errs]
print "diff_errs:", diff_errs
popt, punc, rchi2, dof = general_fit(Hamburg,
                                     np.array(times),
                                     np.array(diffs),
                                     p0,
                                     np.array(diff_errs)) 

print "ga =", popt[0], "+/-", punc[0]
print "NC =", popt[1], "+/-", punc[1]
print "gy =", popt[2], "+/-", punc[2]
print "ta =", popt[3], "+/-", punc[3]
print "ty =", popt[4], "+/-", punc[4]

AnnTimeFit = np.linspace(min(np.array(times)),max(np.array(times)),40000)
DeltaNeffFit = Hamburg(AnnTimeFit, popt[0], popt[1], popt[2], popt[3], popt[4])
full_anneal_time = AnnTimeFit[list(DeltaNeffFit).index(min(DeltaNeffFit))]
print ">>> full anneal time =", full_anneal_time, "mins"


fig1 = plt.figure(1)
ax1 = fig1.add_subplot(111)
ax1.errorbar(times,diffs,yerr=diff_errs,fmt='ro')
ax1.plot(AnnTimeFit,DeltaNeffFit,'b')
ax1.set_title(title)
ax1.set_xlabel("Annealing Time [min@60C]")
ax1.set_ylabel("$N_{eff}$ [$10^{11}\,cm^{-3}$]")
plt.savefig(fig_filename)

plt.show()
