# CVIVutils
A set of utilities for plotting and analyzing data taken at the CVIV setup in the Brown HEP silicon lab.

## Setup:

To get started, clone the repository into the directory containing the measurement campaign you wish to analyze:
```
   $ cd <path_to_data>
   $ git clone <path_to_repo>
```
Within the ```CVIVutils``` directory are two sub-directories, ```analysis``` and ```plotting```

## Use:

### Plotter
To plot the contents of a file, copy one of the example configuration files in the ```plotting``` directory and modify the values to suit your needs. The inputs are all commented with their meaning and how to use them. Make sure the configuration file is in the ```plotting``` directory, as it must be in order to access ```plotter.py```, which does the actual reading and plotting. The ```example_data``` directory contains the data being used in the examples. Your real data files are expected to be in the parent directory that contains ```CVIVutils```. To run the plotter using the configuration file, run ```python IV_example_config.py```.

### CVIVanalyze
CVIVanalyze was originally developed by Sinan Sagir.

To calculate depletion voltages and current from a CVIV measurement, copy one of the example configuration files in the ```analysis``` directory and modify the values to suit your needs. The inputs are all commented with their meanings and how to use them. Each configuration requires a CV and an IV file for the same device. The ```example_data``` directory contains the data being used in the example file. To run the analyzer using the configuration file, run ```python example_config.py```.

### Hamburg fit
To perform a Hamburg fit, copy the file ```hamburg_example.py``` and modify the filenames and input directory to suit your needs. The inputs to the Hamburg fitting script are the ```.dat``` files which are produced by CVIVanalyze. The ```Plots/example_data``` directory contains the data being used in the example. To perform the fit, run ```python hamburg_example.py```.
