import os
import sys
import matplotlib.pyplot as plt
import numpy as np

colors = ['b','g','r','c','m','y','k']

def extract_data(filename,x_column,y_columns,transformation=None):
    with open(filename,'r') as f:
        full_lines = [line for line in f]
        header_idx = [i for i, line in enumerate(full_lines) if 'BiasVoltage' in line][0]
        headers = full_lines[header_idx].split('\t')
        x_idx = headers.index(x_column.keys()[0])
        y_idxs = {name : headers.index(name) for name in y_columns.keys()}

        x_data = {x_column.keys()[0] : []}
        y_data = {name : [] for name in y_columns.keys()}

        data = full_lines[header_idx+1:]
        for line in data:
            if line.strip()[0] == '$':
                continue

            words = line.split()
            x_data[x_column.keys()[0]].append(abs(float(words[x_idx])))
            for key in y_data.keys():
                if transformation:
                    y_data[key].append(transformation(float(words[y_idxs[key]])))
                else:
                    y_data[key].append(float(words[y_idxs[key]]))

        return x_data, y_data

def plot(x_data,x_column,x_range,y_data,y_columns,y_label,y_range,filename):
    fig = plt.figure(1)
    basename = os.path.basename(os.path.normpath(filename))
    fig.suptitle(basename[:-4])
    ax = fig.add_subplot(111)

    for i, key in enumerate(sorted(y_data.keys())):
        print ">>> plotting", key
        ax.plot(x_data[x_data.keys()[0]],
                y_data[key][:len(x_data[x_data.keys()[0]])],
                colors[i]+'o-',
                label = y_columns[key])
        ax.set_ylabel(y_label)
        if x_range:
            ax.set_xlim(x_range)
        if y_range:
            ax.set_ylim(y_range)
        ax.legend(loc='upper left', prop={'size':6})
    print ">>> saving with filename", filename[:-4],'.png'
    plt.savefig(filename[:-4]+'.png')
    plt.show()

def main(filepath, x_column, x_range, y_columns, y_label, y_range, transform):
    xdata, ydata = extract_data(filepath,x_column,y_columns,transform)
    plot(xdata,x_column,x_range,ydata,y_columns,y_label,y_range,filepath)


