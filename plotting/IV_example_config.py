import plotter

# path to the file to be plotted
# it is recommended to use the full path to the file, not one relative to the current dir as shown
filepath = './example_data/IV_HPK_4287_11_-20C_5_24_2018_5_30_07_PM.txt'

# name of the column in the file that will be the x-axis, and its label
x_column = {'BiasVoltage' : 'Bias Voltage (V)'}

# range of values for the x-axis (should be in list form, e.g. [0,1])
# setting this to None leaves x-axis range up to the plotter
x_range = None

# names of the columns in the file that will be plotted against x, along with
# their labels
y_columns = {'Bias Current_Avg' : 'Average Bias Current (A)',
             'Total Current_Avg' : 'Average Total Current (A)'}

# since there can be multiple series plotted on the y-axis, we need a single
# label for the axis itself
y_label = 'Current (A)'

# range of values for the y-axis (should be in list form, e.g. [0,1])
y_range = None

# transformation function to apply to the y-data
transform = None

plotter.main(filepath,x_column,x_range,y_columns,y_label,y_range,transform)
