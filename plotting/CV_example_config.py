import plotter

# defining a transformation function to use later
def transform(capacitance):
    if capacitance == 0.:
        return 0.
    else:
        return 1./(capacitance**2)

# path to the file to be plotted
# it is recommended to use the full path, not one relative to the current dir as shown
filepath = './example_data/CV_HPK_4287_11_-20C_5_24_2018_5_47_21_PM.txt'

# name of the column in the file that will be the x-axis, and its label
x_column = {'BiasVoltage' : 'Bias Voltage (V)'}

# range of values for the x-axis (should be in list form, e.g. [0,1])
# setting this to None leaves x-axis range up to the plotter
x_range = None

# names of the columns in the file that will be plotted against x, along with
# their labels
y_columns = {'LCR_Cp_freq1000.0' : '1/C^2 (1kHz, F^{-2})',
             'LCR_Cp_freq10000.0' : '1/C^2 (10kHz, F^{-2})'}

# since there can be multiple series plotted on the y-axis, we need a single
# label for the axis itself
y_label = '1/C^2 (F^{-2})'

# range of values for the y-axis (should be in list form, e.g. [0,1])
y_range = None

# transformation function to apply to the y-data
transformation = transform

plotter.main(filepath,x_column,x_range,y_columns,y_label,y_range,transformation)
